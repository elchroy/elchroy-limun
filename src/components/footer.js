import React from "react"

const Footer = () => (
  <footer className="py-4 text-center">
    © {new Date().getFullYear()} LUMIN
  </footer>
)

export default Footer
