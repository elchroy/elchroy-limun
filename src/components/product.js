import React from "react"
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

const Product = ({ product }) => {

  const dispatch = useDispatch();
  const currency = useSelector(({ currency }) => currency);
  const {title, image_url, price} = product;
  
  const addToCart = product => {
    dispatch({ type: `OPEN_SIDEBAR` })
    dispatch({ type: `INCREMENT_OR_ADD`, payload: { product } })
  }

  return (
    <div className="product-item gallery-item py-12 px-8 flex flex-col items-center justify-end">
      <img className=" cursor-pointer object-contain" src={image_url} alt="" onClick={() => addToCart(product)}/>
      <h2 className=" cursor-pointer mt-2" onClick={() => addToCart(product)}>{title}</h2>
      <div className="mt-2">From: {currency} {price}</div>
      <div className="flex w-full mt-2 justify-center">
        <button className="action-btn cursor-pointer text-white text-sm py-3 px-5 w-full md:w-1/2 bg-cta hover:bg-cta-2 transition-all duration-300" onClick={() => addToCart(product)}>Add to Cart</button>
      </div>
    </div>
  )
}

Product.propTypes = {
  product: PropTypes.object.isRequired,
}

export default Product
