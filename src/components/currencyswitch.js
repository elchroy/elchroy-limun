import React from "react"
import { useDispatch, useSelector } from "react-redux";

function CurrencySwitch ({ currencies }) {
  const dispatch = useDispatch();
  const currency = useSelector(({ currency }) => currency);

  const updateCurrency = (currency) => {
    dispatch({ type: `SET_CURRENCY`, payload: { currency } })
  }

  return (
    <select className="px-3 py-2 bg-white mb-3 text-sm rounded cursor-pointer " value={currency} name="currency" id="currency" onBlur={e => updateCurrency(e.target.value)} onChange={e => updateCurrency(e.target.value)}>
      { currencies.map((curr, i) => (
        <option value={curr} key={i}>{ curr }</option>
      )) }
    </select>
  )
}

export default CurrencySwitch
