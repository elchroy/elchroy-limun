import React, { useState } from "react"
import PropTypes from "prop-types"
import Header from "./header"
import Footer from "./footer"
import Products from "./products";
import { useSelector, useDispatch } from "react-redux";
import Cart from "./cart";
import helper from '../helper';



const Layout = () => {
  const [currencies, setCurrencies] = useState([]);
  const sidebarOpen = useSelector(({ sidebarOpen }) => sidebarOpen);
  const dispatch = useDispatch();

  helper.getCurrencies()
    .then(({ data: {currency: currencies} }) => { setCurrencies(currencies) })
    .catch(() => dispatch({ type: `API_REQUEST_ERROR` }));

  return (
    <div id="page" className="bg-primary font-sans">
      <Header />
      <div id="main" className="main m-auto">
        <Products />
      </div>
      <div id="sidebar" className={`fixed top-0 w-full right-0 ${(sidebarOpen ? 'open' : 'closed')}`}>
        <div id="sidebar-container" className=" absolute top-0 bg-secondary right-0 overflow-scroll">
          <Cart currencies={currencies} />
        </div>
      </div>
      <Footer />
    </div>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
