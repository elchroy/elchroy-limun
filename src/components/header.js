import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"

const Header = () => {
  return (<header className="fixed w-full">
    <div className="header bg-secondary items-center justify-start flex py-3  px-6 sm:px-20">
      <h1 className="sm:ml-10"> <Link to="/"> LUMIN </Link> </h1>
    </div>
  </header>)
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
