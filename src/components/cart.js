import React from "react"
import { useSelector, useDispatch } from "react-redux"
import CurrencySwitch from "./currencyswitch";

const Cart = ({ currencies }) => {
  const dispatch = useDispatch();
  const cart = useSelector(({ cart }) => cart);
  const currency = useSelector(({ currency }) => currency);
  const products = useSelector(({ products }) => products);
  
  const increment = product => dispatch({ type: `INCREMENT_OR_ADD`, payload: { product } })
  const decrement = product => dispatch({ type: `DECREMENT_OR_REMOVE`, payload: { product } })
  const remove = product => dispatch({ type: `REMOVE_FROM_CART`, payload: { product } })

  let total = 0;

  return (
    <div className=" flex-col pb-0 items-between relative overflow-scroll">
      <div className="px-4 cart-header bg-gray w-full shadow fixed">
        <div className="flex px-4 pt-4 justify-center mb-3">
          <button className="absolute left-4 text-sm" onClick={() => dispatch({ type: 'CLOSE_SIDEBAR'})}>X</button>
          <h1 className="uppercase text-sm">Your Cart</h1>
        </div>
        <CurrencySwitch className="" currencies={currencies} />
      </div>
      <div className="px-4 cart-body pt-28 pb-60">
        {Object.keys(cart).map(k => {
          let product = products[k]
          total += (cart[k].qty * product.price)
          return (
            <div key={k} className="bg-white mb-4 flex relative">
              <span className="absolute top-0 right-0 mr-3 mt-2 cursor-pointer uppercase text-sm" onClick={() => remove(product)}>X</span>
              <div className="w-2/3 py-2 px-4 ">
                <h6>{product.title}</h6>
                <h6>Made For: [TEST_USER]</h6>
                <div className="qty-price w-full flex mt-3 text-sm justify-between">
                  <div className="w-2/3 qty border flex items-stretch justify-center text-center text-sm">
                    <button onClick={() => decrement(product)} className="w-1/3">-</button>
                    <span className=" flex items-center justify-center w-1/3">{cart[k].qty}</span>
                    <button onClick={() => increment(product)} className="w-1/3">+</button>
                  </div>
                  <span className="w-1/3 py-3 "><span className="uppercase">{ currency }</span> { product.price * cart[k].qty}</span>
                </div>
              </div>
              <div className="w-1/3 flex justify-center items-center object-contain">
                <img className="cart-item-img object-contain" src={product.image_url} alt=""/>
              </div>
            </div>
          )
        })}
      </div>
      <div className="px-4 cart-footer bg-gray w-full shadow-lg fixed bottom-0 py-5">
        <div className="flex w-full mx-auto items-stretch text-sm justify-between mt-4">
          <span>Subtotal</span>
          <span className="font-extrabold">{ `${currency} ${total}` }</span>
        </div>
        <button className="uppercase px-4 text-sm bg-white w-full text-cta border border-cta font-light py-4 mt-4 cursor-pointer">MAKE THIS A SUBSCRIPTION (SAVE UP TO 20%)</button>
        <button className="uppercase px-4 text-sm bg-cta w-full text-primary font-light py-4 my-4 cursor-pointer">PROCEED TO CHECKOUT</button>
      </div>
    </div>
  );
};

export default Cart;