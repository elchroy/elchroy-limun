/**
 * Products Container component that queries for products from lumin api
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React, { useEffect } from "react"
import Product from "./product";
import { useSelector, useDispatch } from "react-redux";
import helper from '../helper';

function Products () {

  const currency = useSelector(({ currency }) => currency);
  const products = useSelector(({ products }) => products);
  const dispatch = useDispatch();

  useEffect(() => {
    helper
      .updateProducts(currency)
      .then(({data: {products: _products}}) => {
        let products = _products.reduce((acc, p) => ({...acc, [p.id]: p}), {});
        dispatch({ type: `ALL_PRODUCTS`, payload: { products } })
      })
      .catch(() => dispatch({ type: `API_REQUEST_ERROR` }));
  }, [currency]);

  return (
    <div className="products-grid grid grid-cols-2 sm:grid-cols-3">
      { Object.values(products).map((product, i) => <Product key={i} product={product} />) }
    </div>
  )
}

export default Products
