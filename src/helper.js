import { gql } from '@apollo/client';
import { ApolloClient, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
    uri: 'https://pangaea-interviews.now.sh/api/graphql',
    cache: new InMemoryCache(),
    errorPolicy: 'none'
});

const getCurrencies = () => {
    return client.query({
        query: gql`query CurrenciesQuery { currency }`
    });
}

const updateProducts = currency => {
  return client.query({
    query: gql`query ProductsQuery ($currency: Currency!)  {
      products {
        id
        image_url
        title
        price(currency: $currency)
      }
    }`,
    variables: { currency }
  })
}

export default {
    getCurrencies,
    updateProducts
};