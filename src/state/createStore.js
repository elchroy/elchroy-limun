import { createStore as reduxCreateStore } from "redux"

const reducer = (state, { type, payload }) => {
    switch (type) {
        case 'OPEN_SIDEBAR':
            return { ...state, sidebarOpen: true }
        case 'CLOSE_SIDEBAR':
            return { ...state, sidebarOpen: false }
        case 'INCREMENT_OR_ADD':
            return {
                ...state,
                cart: {
                    ...state.cart,
                    [payload.product.id]: {
                        qty: state.cart[payload.product.id] == null ? 1 : state.cart[payload.product.id].qty + 1
                    }
                }
            }
        
        case 'ALL_PRODUCTS':
            return { ...state, error: false, products: payload.products }
        
        case 'API_REQUEST_ERROR':
            return { ...state, error: true }
        
        case 'SET_CURRENCY':
            return { ...state, error: false, currency: payload.currency }

        case 'DECREMENT_OR_REMOVE':
            let qty = (state.cart[payload.product.id] == null)
                ? 0 : Math.max(0, state.cart[payload.product.id].qty - 1);
            if (qty === 0) {
                const { [payload.product.id]: val, ...withoutProduct } = state.cart;
                return { ...state, cart: withoutProduct }
            }
            return {
                ...state,
                cart: { ...state.cart, [payload.product.id]: {
                    qty
                } }
            }

        case 'REMOVE_FROM_CART':
            const { [payload.product.id]: val, ...withoutProduct } = state.cart;
            return { ...state, cart: withoutProduct }
        default:
            return state;
    }
}

const initialState = { error: false, sidebarOpen: false, currency: "USD", cart: {}, products: {} }

const createStore = () => reduxCreateStore(reducer, initialState)

export default createStore