module.exports = {
  // purge: [],
  purge: ['./src/**/*.{js,jsx,ts,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    screens: {
      sm: '768px',
      md: '992px'
    },
    fontFamily: {
      sans: ['Signika', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
    colors: {
      transparent: 'transparent',
      primary: '#e2e6e3',
      secondary: '#fcfcf9',
      gray: '#f2f2ef',
      black: '#000',
      white: '#fff',
      danger: '#69141b',
      cta: "#4b5548",
      "cta-2": "#2b2e2b"
    }
  },

  
  variants: {
    extend: {},
  },
  plugins: [],
}
