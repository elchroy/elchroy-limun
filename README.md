<h1 align="center">Lumin Clone</h1>

_This project was built with GatsbyJS, ApolloClient, TailwindCSS, amongst other tools.__

## Quick setup

1.  **Clone the repository.**

    Use the Gatsby CLI to create a new site, specifying the default starter.

    ```shell
    git clone https://github.com/elchroy/elchroy-limun.git
    cd elchroy-limun/
    ```

1.  **Install dependencies.**

    Navigate into your new site’s directory and run.

    ```shell 
    npm install
    yarn
    ```

1.  **Start up the application to serve locally!**
    ```shell
    gatsby build
    gatsby serve
    ```

1.  **Open the source code and start editing!**  
    Your site is now running at `http://localhost:9000`!